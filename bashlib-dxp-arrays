# ---------------------------------------------------------------------
# Script   : bashlib-dxp-arrays
# Descrição: Coleção de funções para trabalho com arrays em Bash.
# Versão   : 0.0.1
# Data     : 23.08.2020
# Licença  : GNU/GPL v3.0
# ---------------------------------------------------------------------
# Aviso! Ainda em desenvolvimento, não use em produção!
# ---------------------------------------------------------------------
# Copyright (C) 2020  Blau Araujo <blau@debxp.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------
# Uso: dxp_include math
# ---------------------------------------------------------------------
# Colaboradores:
# 
#   Paulo Oliveira Kretcheu
#   @joseafga
#
# ---------------------------------------------------------------------


bar_list() {

    local cols=$1
    local row=0
    local col=0
    local arr=("${@:2}")
    local n colval

    for ((n = 0; n < "${#arr[@]}"; n++)); do
        [[ $col -eq $cols ]] && { col=0; ((row++)); }
        colval=$(bar_val "$cols:$row:$col" "${arr[@]}")
        [[ $colval ]] && echo "($row,$col) $colval"
        ((col++))
    done
}

bar_dump() {

    local cols=$1
    local row=0
    local col=0
    local arr=("${@:2}")
    local i colval

    echo 'Array ('
    for ((i = 0; i < "${#arr[@]}"; i = i + $cols)); do
        echo "    [$row] => ("
        for ((col = 0; col < $cols; col++)); do
            colval=$(bar_val "$cols:$row:$col" "${arr[@]}")
            [[ $colval ]] && echo "        [$col] => $colval"
        done
        echo '    )'
        ((row++))
    done
    echo ')'
}

bar_val() {

    local cols=${1:0:1}
    local row=${1:2:1}
    local col=${1:4:1}
    local arr=("${@:2}")
    local e="$cols*$row+$col"

    [[ "${arr[$e]}" ]] && echo "${arr[$e]}" || return 1
}

explode() {
    declare -g arr=$1
    local sep="$2"
    local str="$3"

    mapfile -td "$sep" arr <<< "$str"
    arr[${#arr[@]} - 1]="${arr[${#arr[@]} - 1]::-1}"
}

implode() {
    declare -n var=$1
    local sep="$2"
    local arr=("${@:3}")

    var="${arr[@]/%/$sep}"
    var="${var//$sep /$sep}"
    var="${var::-${#sep}}"
}
